#include "HashCoder.h"

namespace hashcoder {

void FactoryHashCoder::registerHashCoder(const std::string& id, HashCoder_uptr hashcoder) {
	_listHascoder.insert(std::make_pair(id, std::move(hashcoder)));
}

FactoryHashCoder::HashCoder FactoryHashCoder::getHasCoder(const std::string& id) const {
	return _listHascoder.at(id).get();
}

}