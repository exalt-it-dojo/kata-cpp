﻿#pragma once

#include <string>
#include <utility>
#include <list>
#include <set>
#include <map>
#include <algorithm>
#include <thread>
#include <mutex>
#include <vector>
#include <optional>
#include <memory>
#include <unordered_map>
#include <functional>

namespace hashcoder
{

#define IHashCoderTemplated IHashCoder<GeneratorStr, GeneratorHash>
#define HashCoderTemplated HashCoderSample<GeneratorStr, GeneratorHash>

  using PairHashCodListStr = std::pair<std::string, std::set<std::string>>;
  using CollHashCodeListStr = std::map<std::string, std::set<std::string>>;

  template <typename GeneratorStr, typename GeneratorHash>
  class IHashCoder
  {

  public:
    // Constructor
    IHashCoder(GeneratorStr aGeneratorStr, GeneratorHash aGeneratorHash, int nbrString) : _generatorStr(aGeneratorStr), _generatorHash(aGeneratorHash), _nbrStringToFind(nbrString)
    {
    }

    // destructeur
    virtual ~IHashCoder(){};

    // functions
    virtual PairHashCodListStr genPairHashCodeStr() = 0;
    virtual bool end(const std::set<std::string> &listString) const = 0;

  protected:
    // fonction de génération du string
    GeneratorStr _generatorStr;
    // fonction de génération du hash
    GeneratorHash _generatorHash;
    // nbr de string avec même hash à trouver
    int _nbrStringToFind;

    virtual std::string genStr() { return _generatorStr(); }
    virtual std::string genHash(const std::string &str) const { return _generatorHash(str); };
  };

  template <typename GeneratorStr, typename GeneratorHash>
  class HashCoderSample : public IHashCoder<GeneratorStr, GeneratorHash>
  {

  public:
    // constructor
    HashCoderSample() = delete;
    HashCoderSample(GeneratorStr aGeneratorStr, GeneratorHash aGeneratorHash, int nbrString) : IHashCoderTemplated(aGeneratorStr, aGeneratorHash, nbrString)
    {
    }

    // destructor
    ~HashCoderSample() {}

    // functions
    virtual PairHashCodListStr genPairHashCodeStr()
    {
      PairHashCodListStr pairHashListStr;

      do
      {
        std::string strGenerate = IHashCoderTemplated::genStr();
        std::string hashGenerate = IHashCoderTemplated::genHash(strGenerate);

        pairHashListStr = _add_pair_hash_str(strGenerate, hashGenerate);
      } while (!end(pairHashListStr.second));

      return pairHashListStr;
    }

  private:
    // collection de paire hash et liste de strings
    CollHashCodeListStr _collPairHashStr;

    // méthode privée qui va ajouter le string en paramètre a la collection s'il existe
    // sinon va ajouter la paire
    virtual PairHashCodListStr
    _add_pair_hash_str(const std::string &str, const std::string &hashCode)
    {
      auto itPairHashListeStr = _collPairHashStr.find(hashCode);
      if (itPairHashListeStr != _collPairHashStr.end())
      {
        itPairHashListeStr->second.insert(str);
      }
      else
      {
        itPairHashListeStr = _collPairHashStr.insert(
                              std::make_pair(hashCode, std::set<std::string>{str})
                            ).first;
      }

      return *itPairHashListeStr;
    }

    virtual bool end(const std::set<std::string> &listString) const
    {
      return listString.size() == IHashCoderTemplated::_nbrStringToFind;
    };
  };

  // la factory
  class FactoryHashCoder
  {
    using FunctorGenStr = std::function<std::string(void)>;
    using FunctorGenHash = std::function<std::string(std::string)>;
    using HashCoder_uptr = std::unique_ptr<IHashCoder<FunctorGenStr, FunctorGenHash>>;
    using HashCoder = IHashCoder<FunctorGenStr, FunctorGenHash> *;

  public:
    static FactoryHashCoder &getInstance()
    {
      static FactoryHashCoder _factory;
      return _factory;
    }

    void registerHashCoder(const std::string &id, HashCoder_uptr hashcoder);

    HashCoder getHasCoder(const std::string &id) const;

  private:
    std::map<std::string, HashCoder_uptr> _listHascoder;
  };

}