﻿# CMakeList.txt : fichier projet CMake de niveau supérieur, effectuez une configuration globale
# et incluez les sous-projets ici.
#
cmake_minimum_required (VERSION 3.8)

# GoogleTest requires at least C++11
set(CMAKE_CXX_STANDARD 20)

project ("3SameHashCode")

# Incluez les sous-projets.
add_subdirectory ("hashcoder")

# link library to exe
function(needHashCoder targetLink)
	target_link_libraries(${targetLink} PUBLIC hashCoderLib Threads::Threads)
endfunction()

set(THREADS_PREFER_PTHREAD_FLAG ON)
find_package(Threads REQUIRED)


# main source.
add_executable (${PROJECT_NAME} "Main.cpp")

# link library
needHashCoder(${PROJECT_NAME})

option(gtest_disable_pthreads "Disable uses of pthreads in gtest." on)

include(FetchContent)
FetchContent_Declare(
  googletest
  GIT_REPOSITORY https://github.com/google/googletest.git
  GIT_TAG        release-1.11.0
)
# For Windows: Prevent overriding the parent project's compiler/linker settings
set(gtest_force_shared_crt ON CACHE BOOL "" FORCE)
FetchContent_MakeAvailable(googletest)

enable_testing()


add_executable(
   hashcoderTest
   "test/hashCoderTest.cpp"
)

# link library
needHashCoder(hashcoderTest)
target_link_libraries(
	hashcoderTest
	PUBLIC
	gtest_main
)

include(GoogleTest)
gtest_discover_tests(hashcoderTest)

add_test(hashcoderTest hashcoderTest)
