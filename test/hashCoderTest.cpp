#include <gtest/gtest.h>
#include <fstream>
#include <random>
#include <iostream>

#include "HashCoder.h"

using LambdaGenStr = std::function<std::string(void)>;
using LambdaGenHash = std::function<std::string(std::string)>;

int i = 0;

// lambda pour générer un string
// random ou autre
auto lambdaGenStr = []() -> std::string {

	return std::to_string(i++);
};

// on créé notre propre méthode de hash (une méthode pas efficace... le but c'est pas de faire sha256...) 
// une idée comme ça, réduire la taille de la chaine en sortie de std::hash ;)
auto lambdaGenHash = [](std::string str) -> std::string {
	std::ostringstream oss;
	oss << std::hash<std::string>{}(str);
	return oss.str().substr(0, 10);
};

class HashCoderTest : public testing::Test
{

public:
	// test class constructeur
	HashCoderTest() {
		//_factory = hashcoder::FactoryHashCoder::getInstance();
	};

	// setup call at beginning of each tests
	void SetUp() {i=0;};
	// tearUp call at end of each tests
	void tearUp() {};

protected:
	//hashcoder::FactoryHashCoder _factory;
};

// test unitaire pour vérifier que nos 3 strings sont identique mais
// on un même hash généré
// ex :
// toto -> hash 1a2bC...
// tata -> hash 1a2bC...
// titi -> hash 1a2bC...
TEST_F(HashCoderTest, generate_3_strings_same_hashcode)
{
	// ici faudra definir le type PairHashCodListStr.
	hashcoder::HashCoderSample<LambdaGenStr, LambdaGenHash> hashCoderTest(lambdaGenStr, lambdaGenHash, 3);
	hashcoder::PairHashCodListStr resPairs = hashCoderTest.genPairHashCodeStr();
	
	// on récupère la liste de string (normalement ils ont tous le même hash code)
	std::set<std::string> resListString = resPairs.second;
	// 3 strings doivent se trouver dans la liste (pas moins pas plus)
	EXPECT_EQ(resListString.size(), 3);

	std::cout << "hash [" << resPairs.first << "]" << std::endl; 

	// lambda pour vérifier que les strings dans la liste génère bien le même hash
	auto expectSameLambda = [&](const std::string& str) { 
		// le calcul du hash ne doit pas varier dans le temps...
		std::cout << "str [" << str << "]" << std::endl;
		EXPECT_EQ(lambdaGenHash(str), resPairs.first);
	};

	// pour tous les strings générés on passe notre petit lambda
	std::for_each(resListString.begin(), resListString.end(), expectSameLambda);
}

TEST_F(HashCoderTest, test_factory)
{
	hashcoder::FactoryHashCoder& _factory = hashcoder::FactoryHashCoder::getInstance();	
	hashcoder::HashCoderSample<LambdaGenStr, LambdaGenHash> hashCoderTest(lambdaGenStr, lambdaGenHash, 3);
	
	_factory.registerHashCoder("id001", std::make_unique<hashcoder::HashCoderSample<LambdaGenStr, LambdaGenHash>>(hashCoderTest));

	hashcoder::PairHashCodListStr resPairs = _factory.getHasCoder("id001")->genPairHashCodeStr();
	
	// on récupère la liste de string (normalement ils ont tous le même hash code)
	std::set<std::string> resListString = resPairs.second;
	// 3 strings doivent se trouver dans la liste (pas moins pas plus)
	EXPECT_EQ(resListString.size(), 3);

	std::cout << "hash [" << resPairs.first << "]" << std::endl; 

	// lambda pour vérifier que les strings dans la liste génère bien le même hash
	auto expectSameLambda = [&](const std::string& str) { 
		// le calcul du hash ne doit pas varier dans le temps...
		std::cout << "str [" << str << "]" << std::endl;
		EXPECT_EQ(lambdaGenHash(str), resPairs.first);
	};

	// pour tous les strings générés on passe notre petit lambda
	std::for_each(resListString.begin(), resListString.end(), expectSameLambda);
}